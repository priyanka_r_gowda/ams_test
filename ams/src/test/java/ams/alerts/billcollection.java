package ams.alerts;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class billcollection {
	private WebDriver driver;
	JavascriptExecutor js;

	@BeforeTest
	public void setUp() {
		System.setProperty("webdriver.chrome.driver", "C://chromedriver.exe");
		driver = new ChromeDriver();
		js = (JavascriptExecutor) driver;
	}

	@AfterTest
	public void tearDown() {
		driver.quit();
	}
	@Test
	public void f() {
		driver.get("http://68.183.244.87:8080/");
		driver.manage().window().maximize();
		driver.findElement(By.id("username")).click();
		driver.findElement(By.id("username")).sendKeys("0349@bpec.tg");
		driver.findElement(By.id("password")).sendKeys("honore123$");
		WebElement login =driver.findElement(By.xpath("/html/body/section/div[2]/div/div/div/div/div[3]/form/div[3]/div[2]/button"));
		js.executeScript("arguments[0].click();", login);
		
		//Bill collection
		WebElement coll = driver.findElement(By.xpath("/html/body/div[1]/header/nav/ul/li[6]/a/b"));
		js.executeScript("arguments[0].click();", coll);
		WebElement billcol = driver.findElement(By.xpath("/html/body/div[1]/header/nav/ul/li[6]/ul/li[2]/a"));
		js.executeScript("arguments[0].click();", billcol);
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		WebElement collect = driver.findElement(By.xpath("/html/body/div[2]/div/div/div[3]/div/div/div/div[1]/div[1]/a/span"));
		js.executeScript("arguments[0].click();", collect);
		driver.manage().timeouts().implicitlyWait(200, TimeUnit.SECONDS);
		
		WebElement fileInput = driver.findElement(By.name("cbsToamsFile"));
		fileInput.sendKeys("C://Users/Admin/Desktop/CBS_AMS_o374mdsvb7hpfa9q.txt");
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		driver.findElement(By.id("uploadFileBtn")).submit();
		WebElement email_id= driver.findElement(By.xpath("/html/body/div[5]/div/div[2]/button"));
		email_id.click();
		driver.manage().timeouts().implicitlyWait(500, TimeUnit.SECONDS);	
		System.out.println("File uploaded");
		
		//validate the uploaded file
		WebElement validate = driver.findElement(By.xpath("/html/body/div[2]/div/div/form/div/div/div/div/div[2]/div/a[1]/button/span"));
		js.executeScript("arguments[0].click();", validate);
		driver.manage().timeouts().implicitlyWait(500, TimeUnit.SECONDS);
		WebElement email_id1= driver.findElement(By.xpath("/html/body/div[5]/div/div[2]/button"));
		js.executeScript("arguments[0].click();", email_id1);
		driver.manage().timeouts().implicitlyWait(500, TimeUnit.SECONDS);
		System.out.println("File validated");
		
		//Submit the records to backend.
		WebElement element = driver.findElement(By.xpath("//*[@id='saveToMainTableOfUserBtn']"));
		js.executeScript("arguments[0].click();", element);
		driver.manage().timeouts().implicitlyWait(500, TimeUnit.SECONDS);
		WebElement email_id2= driver.findElement(By.xpath("/html/body/div[5]/div/div[2]/button"));
		js.executeScript("arguments[0].click();", email_id2);
		driver.manage().timeouts().implicitlyWait(500, TimeUnit.SECONDS);
		System.out.println("File submitted");
	}
}
