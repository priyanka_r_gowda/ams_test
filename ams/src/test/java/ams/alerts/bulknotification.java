package ams.alerts;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class bulknotification {
	private WebDriver driver;
	JavascriptExecutor js;

	@BeforeTest
	public void setUp() {
		System.setProperty("webdriver.chrome.driver", "C://chromedriver.exe");
		driver = new ChromeDriver();
		js = (JavascriptExecutor) driver;
	}

	@AfterTest
	public void tearDown() {
		driver.quit();
	}

	@Test
	public void afterTest() {

		driver.get("http://68.183.244.87:8080/");
		driver.manage().window().maximize();
		driver.findElement(By.id("username")).click();
		driver.findElement(By.id("username")).sendKeys("0349@bpec.tg");
		driver.findElement(By.id("password")).sendKeys("honore123$");
		WebElement login =driver.findElement(By.xpath("/html/body/section/div[2]/div/div/div/div/div[3]/form/div[3]/div[2]/button"));
		js.executeScript("arguments[0].click();", login);

		//send bulk notification
		WebElement noti = driver.findElement(By.xpath("/html/body/div[1]/header/nav/ul/li[5]/a/b"));
		js.executeScript("arguments[0].click();", noti);
		WebElement bulk = driver.findElement(By.xpath("/html/body/div[1]/header/nav/ul/li[5]/ul/li[2]/a"));
		js.executeScript("arguments[0].click();", bulk);
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		WebElement fileInput = driver.findElement(By.name("fileData"));
		fileInput.sendKeys("C://Users/Admin/Desktop/Bulk_Template_10.csv");
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		driver.findElement(By.id("BulkNotifysend")).submit();
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		driver.findElement(By.id("BulkNotifysend")).submit();
		WebElement email_id= driver.findElement(By.className("dlg-wrapper"));
		email_id.click();
	}

}
