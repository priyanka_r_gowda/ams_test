package ams.alerts;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class bulksubscription {
	private WebDriver driver;
	JavascriptExecutor js;

	@BeforeTest
	public void setUp() {
		System.setProperty("webdriver.chrome.driver", "C://chromedriver.exe");
		driver = new ChromeDriver();
		js = (JavascriptExecutor) driver;
		}

	@AfterTest
	public void tearDown() {
		driver.quit();
	}
	@Test
	public void bulksubs() {

		driver.get("http://68.183.244.87:8080/");
		driver.manage().window().maximize();
		driver.findElement(By.id("username")).click();
		driver.findElement(By.id("username")).sendKeys("0349@bpec.tg");
		driver.findElement(By.id("password")).sendKeys("honore123$");
		WebElement login =driver.findElement(By.xpath("/html/body/section/div[2]/div/div/div/div/div[3]/form/div[3]/div[2]/button"));
		js.executeScript("arguments[0].click();", login);
		driver.manage().timeouts().implicitlyWait(500, TimeUnit.SECONDS);	
		WebElement subs = driver.findElement(By.xpath("/html/body/div[1]/header/nav/ul/li[4]/a/b"));
		js.executeScript("arguments[0].click();", subs);
		WebElement bulk =driver.findElement(By.xpath("/html/body/div[2]/div/div/div[2]/div/div/div/div[1]/div[1]/a[1]/span"));
		js.executeScript("arguments[0].click();", bulk);
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		WebElement fileInput = driver.findElement(By.name("customerFile"));
		fileInput.sendKeys("C://Users/Admin/Desktop/bulk_subs_sample_fr.csv");
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		driver.findElement(By.id("uploadFileBtn")).submit();
		WebElement email_id= driver.findElement(By.xpath("/html/body/div[4]/div/div[2]/button"));
		email_id.click();
		driver.manage().timeouts().implicitlyWait(500, TimeUnit.SECONDS);	
		
		//validate the uploaded file
		WebElement validate = driver.findElement(By.xpath("/html/body/div[2]/div/div/form/div/div/div/div/div[3]/div/a[1]/button/span"));
		js.executeScript("arguments[0].click();", validate);
		WebElement email_id1= driver.findElement(By.xpath("/html/body/div[4]/div/div[2]/button"));
		js.executeScript("arguments[0].click();", email_id1);
		driver.manage().timeouts().implicitlyWait(500, TimeUnit.SECONDS);
		
		//Submit the records to backend.
		WebElement element = driver.findElement(By.id("saveToMainTableOfUserBtn"));
		Actions builder = new Actions(driver);
		builder.moveToElement(element, 79, 29).click().build().perform();
		WebElement email_id2= driver.findElement(By.xpath("/html/body/div[4]/div/div[2]/button"));
		js.executeScript("arguments[0].click();", email_id2);
		driver.manage().timeouts().implicitlyWait(500, TimeUnit.SECONDS);
		
	}

}
