package ams.alerts;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class singlenotification {
	private WebDriver driver;
	JavascriptExecutor js;

	@BeforeTest
	public void setUp() {
		System.setProperty("webdriver.chrome.driver", "C://chromedriver.exe");
		driver = new ChromeDriver();
		js = (JavascriptExecutor) driver;
		
	}

	@AfterTest
	public void tearDown() {
		driver.quit();
	}

	@Test
	public void singlenotify() {
		driver.get("http://68.183.244.87:8080/");
		driver.manage().window().maximize();
		driver.findElement(By.id("username")).click();
		driver.findElement(By.id("username")).sendKeys("0349@bpec.tg");
		driver.findElement(By.id("password")).sendKeys("honore123$");
		WebElement login =driver.findElement(By.xpath("/html/body/section/div[2]/div/div/div/div/div[3]/form/div[3]/div[2]/button"));
		js.executeScript("arguments[0].click();", login);
		
		//send a single notification
		WebElement noti = driver.findElement(By.xpath("/html/body/div[1]/header/nav/ul/li[5]/a/b"));
		js.executeScript("arguments[0].click();", noti);
		WebElement single = driver.findElement(By.xpath("/html/body/div[1]/header/nav/ul/li[5]/ul/li[1]/a"));
		js.executeScript("arguments[0].click();", single);
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		WebElement plus =driver.findElement(By.className("collapsed"));
		js.executeScript("arguments[0].click();", plus);
		driver.findElement(By.id("getaccnum")).sendKeys("15104370004");
		WebElement search = driver.findElement(By.id("getAllCustomerId"));	
		js.executeScript("arguments[0].click();", search);
		js.executeScript("window.scrollBy(0,1000)");
		driver.findElement(By.xpath("//*[@id=\"btn-filter\"]")).submit();
		WebElement email_id= driver.findElement(By.className("dlg-wrapper"));
		email_id.click();
		
	}

}
