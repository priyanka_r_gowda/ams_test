package ams.alerts;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterTest;

public class singlesubscription {
	private WebDriver driver;
	JavascriptExecutor js;

	@BeforeTest
	public void setUp() {
		System.setProperty("webdriver.chrome.driver", "C://chromedriver.exe");
		driver = new ChromeDriver();
		js = (JavascriptExecutor) driver;
		
	}

	@AfterTest
	public void tearDown() {
		driver.quit();
	}

	@Test
	public void singsubs() {
		driver.get("http://68.183.244.87:8080/");
		driver.manage().window().maximize();
		driver.findElement(By.id("username")).click();
		driver.findElement(By.id("username")).sendKeys("0349@bpec.tg");
		driver.findElement(By.id("password")).sendKeys("honore123$");
		WebElement login =driver.findElement(By.xpath("/html/body/section/div[2]/div/div/div/div/div[3]/form/div[3]/div[2]/button"));
		js.executeScript("arguments[0].click();", login);
		driver.manage().timeouts().implicitlyWait(500, TimeUnit.SECONDS);	
		WebElement subs = driver.findElement(By.xpath("/html/body/div[1]/header/nav/ul/li[4]/a/b"));
		js.executeScript("arguments[0].click();", subs);
		WebElement single =driver.findElement(By.xpath("/html/body/div[2]/div/div/div[2]/div/div/div/div[1]/div[1]/a[2]/span"));
		js.executeScript("arguments[0].click();", single);
		driver.findElement(By.name("cid")).sendKeys("121");
		driver.findElement(By.name("accnum")).sendKeys("10101010105");
		driver.findElement(By.id("title")).sendKeys("Mr");
		driver.findElement(By.id("fname")).sendKeys("John");
		driver.findElement(By.id("lname")).sendKeys("Vincent");
		driver.findElement(By.id("email")).sendKeys("john@gmail.com");
		driver.findElement(By.id("mobile")).sendKeys("90878907");
		WebElement dropdown1 = driver.findElement(By.id("periodicity"));
		dropdown1.findElement(By.xpath("//option[. = 'Mensuelle']")).click();
		
		driver.findElement(By.id("extsms")).click();
		{
			WebElement dropdown = driver.findElement(By.id("extsms"));
			dropdown.findElement(By.xpath("//option[. = 'OUI']")).click();
		}
		
		driver.findElement(By.id("pname")).click();
		{
			WebElement dropdown = driver.findElement(By.id("pname"));
			dropdown.findElement(By.xpath("//option[. = 'BPEC Alertes Personnel BPEC']")).click();
		}
		driver.findElement(By.id("language")).click();
		{
			WebElement dropdown = driver.findElement(By.id("language"));
			dropdown.findElement(By.xpath("//option[. = 'Anglais']")).click();
		}
		driver.findElement(By.id("benq")).click();
		{
			WebElement dropdown = driver.findElement(By.id("benq"));
			dropdown.findElement(By.xpath("//option[. = 'Aucun']")).click();
		}
		driver.findElement(By.id("benqday")).click();
		{
			WebElement dropdown = driver.findElement(By.id("benqday"));
			dropdown.findElement(By.xpath("//option[. = 'Dim']")).click();
		}
		Select drpCountry = new Select(driver.findElement(By.xpath("//*[@id=\"minist\"]")));
		drpCountry.selectByVisibleText("Aucun");
		
		Select drpday = new Select(driver.findElement(By.xpath("//*[@id='ministday']")));
		drpday.selectByVisibleText("Lun");
		
		
		driver.findElement(By.id("lwlimit")).sendKeys("2000");
		driver.findElement(By.id("uplimit")).sendKeys("30000");
		driver.findElement(By.cssSelector(".btn-success > span")).click();
		driver.findElement(By.cssSelector(".dlg-action")).click();
		
		
	}

}
